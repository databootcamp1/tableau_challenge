# Module 18 Tableau Challenge
Creator: Melissa Acevedo

### Data Source
Monthly CSV files for the year 2020, collected from [Citi Bike Data](https://citibikenyc.com/system-data) webpage.

### Visualizations
[https://public.tableau.com/app/profile/melissa.acevedo/viz/CitiBikeData_17097937696510/Dashboard1?publish=yes]

## Analysis of CitiBike Data

## Top Starting Locations: 

- **Most Frequent Starting Point:** 12 Ave & W 40 St
  - This station stands out as the most frequently used starting point, indicating high demand in that area.
- **Other Popular Starting Points:**
  - West St & Chambers St
  - Broadway & W 60 St
  - Likely due to central/strategic locations and good connectivity.

## Top Ending Stations: 

- **Consistently Popular:**
  - 12 Ave & W 40 St is popular as both a starting and ending point.
- **Observation:**
  - The most popular locations to start a bike trip are also the most popular locations to end the journey.

![Top Ten](top_ten.png)

## Bottom Ten Start Stations

- **LOW USAGE:**
  - OLD Southern Blvd & E 142 St
    - Lower usage, likely due to its location.
  - Garrison Ave & Manida St
    - Less frequented possibly due to its limited accessibility.

## Bottom Ten End Stations

- **LOW DEMAND:**
  - McGinley Square
  - Communipaw & Berry Lane
- **Observation:**
  - Low demand due to location.

![Bottom Ten](bottom_ten.png)

## General Observations

- **Top Stations:**
  - Often situated in central or strategic locations, contributing to high usage.
- **Bottom Stations:**
  - Lower usage influenced by factors like location, connectivity, or less popularity of surrounding areas.

## Age Analysis

- **Most Frequent Age: 51 Years Old**
  - The most frequently observed age among riders is 51 years old. The age group of 51 years old stands out as having the highest representation in the bike-sharing data.
- **Second Most Popular Age Range: 25 - 35 Years Old**
  - Following closely in popularity is the age range of 25 to 35 years old. This indicates a significant proportion of riders within the younger adult demographic.
  
  ## Observations
  - The popularity of 51 years old might be influenced by factors such as leisurely rides, fitness activities, or commuting patterns of individuals in this age group.
  - The strong presence of riders in the 25-35 age range is in line with the trend observed in many bike-sharing systems, where young adults often use bikes for daily commuting or short trips.


## Monthly Analysis of Bike Trips

- **Peak Season: April to August**
  - The data indicates a clear peak season for bike trips, spanning from April to August. This period experiences the highest levels of bike-sharing activity which can be attributed to the warmer weather typically experienced in spring and summer. Riders are more inclined to use bikes for commuting, recreation, and outdoor activities during milder temperatures. Another factor could be the longer daylight hours. Extended daylight encourages riders to take advantage of the outdoors after work or during weekends.

## Subscriber Dominance

- **Trend:**
  - The data reveals a trend where subscribers slightly outnumber customers in terms of bike usage. This suggests that the bike-sharing system has a strong and consistent user base of subscribers.
- **Observation:**
  - While subscribers dominate, the presence of customers indicates that there is a segment of users who prefer occasional or one-time use of the bike-sharing service. Customers may include tourists, occasional riders, or those who do not require frequent bike access.

![Additional Analysis](additional_analysis.png)

## Station Map
- The map shows all the bike stations used in 2020. The popularity of each station is highlighted by the intensity of the color of the circle. The most used appeared to be around Manhattan area

![Station Map](station_map.png)












